terraform {
  backend "s3" {
    bucket         = "nikitasg-terraform-state"
    key            = "production/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "tfstatelock"
  }
}