# RiskN Demo


## Description

CI/CD pipeline to deploy a nginx webserver (EC2 instance) to the default VPC. 

Tools used:
- Gitlab 
    - The Repo serves as the foundational element, acting as the central source of truth for both the application's code and its configuration.
    - The CD pipeline automates the processes of building, testing, and deploying the application.
- Terraform
    - It takes charge of deploying the application into the desired environment. It handles the orchestration and management of application resources.


## Steps required

First we need to define that security keys as CI/CD environment variables that can be used by the deployment pipeline  **(Project settings -> Ci/CD -> Variables).**

<img src="./images/variables.png">

- **Create the Terraform configuration files**
In this step, we will create the Terraform configuration in the repository. As mentioned earlier, we will create an EC2 instance, running Nginx, in AWS using Terraform and Gitlab pipelines.

- **Create GitLab pipeline using .gitlab-ci.yml**
At this point, our Terraform configuration is ready. Now before pushing the code to our Gitlab repository, we should create the gitlab-ci.yaml file in the same repository, in order to automate the entire workflow .

The pipeline consists of multiple stages, that are triggered automatically when we merge changes to the relevant branch. **Except the deploy stage that has to be run manually.**

Next, we have to declare the stage names and defined them in a sequence. 

Stages:
  - prepare - *initialize Terraform*
  - validate - *validates the code.*
  - test - *tests the code by including an IaC scanning job.*
  - build - *run the plan.*
  - deploy - *This depends on the Plan and consists of Apply and Destroy commands (manual).*


We are using a remote S3 backend with DynamoDb and state locking.
State locking is critical in preventing state conflicts when multiple users or processes interact with the same Terraform configuration. When Terraform plans or applies changes, it locks the state to prevent others from making concurrent changes


Now we are ready to push our code to the repo.
Depenfing on the Branch that we are going to commit, the relevant pipeline is going to be trigerered.

<img src="./images/staging.png">

and 

<img src="./images/planproduction.png">

All jobs were run successfuly. The next step is to deploy our webserver by triggering *manualy* the next stage (apply).

<img src="./images/prodpipeline.png">


After a few minutes our webserver has been created.


<img src="./images/tf-apply.png">


And can be accessed by typing the IP


<img src="./images/test-page.png">

<img src="./images/ec2.png">

<img src="./images/s3.png">

This is a simple example of automating the deployent using Gitlab and Terraform. Same result could be achieved by using AWS Native tools like CodeBuild, Codepipeline and Cloudformation. Or Githb Actions, CircleCI.

We are using multi env deployment, Staging and Production 

For this demo, the webserver is deployed in the default VPC (Public Subnet). For best prectices, we would deploy the server in a Private VPC, behind an ALB, with a Cert obtained by CM and configured with HTTPS access.